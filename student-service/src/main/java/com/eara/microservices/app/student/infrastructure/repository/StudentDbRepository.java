package com.eara.microservices.app.student.infrastructure.repository;

import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.repository.StudentRepository;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;
import com.eara.microservices.app.student.infrastructure.entity.StudentEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StudentDbRepository implements StudentRepository {

    private final StudentJpaRepository studentRepository;

    @Autowired
    public StudentDbRepository(StudentJpaRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public Optional<Student> getStudentById(StudentIdentifier studentId) {
        Optional<Student> student = Optional.of(studentRepository.findById(studentId.getValue()).get().toStudent());
        return student;
    }

    @Override
    public List<Student> getAllStudents() {

        if (StreamSupport.stream(studentRepository.findAll().spliterator(), false).count() != 0) {
            return StreamSupport.stream(studentRepository.findAll().spliterator(), false)
                    .map(item -> item.toStudent())
                    .collect(Collectors.toList());
        }
        else {
            return Collections.emptyList();
        }
    }

    @Override
    public Student save(Student student) {
        Student studentSaved = studentRepository.save(new StudentEntity(student)).toStudent();
        return studentSaved;
    }

    @Override
    public Student update(StudentIdentifier studentId, Student student) {
        return null;
    }

    @Override
    public boolean delete(StudentIdentifier id) {
        return false;
    }
}
