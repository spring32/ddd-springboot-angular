package com.eara.microservices.app.student.application.rest;

import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.service.StudentService;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api", produces = "application/json")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {

        this.studentService = studentService;

    }

    @GetMapping(path = StudentLinks.LIST_STUDENTS)
    public ResponseEntity<List<Student>> getAllStudents() {

        List<Student> students = studentService.getStudents();

        return ResponseEntity.ok(students);
    }

    @GetMapping(path = StudentLinks.GET_STUDENT)
    public ResponseEntity getStudentById(@PathVariable Long studentId) {

        return studentService.getStudentById(new StudentIdentifier(studentId))
                .map(student -> ResponseEntity.ok(student))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(path = StudentLinks.ADD_STUDENTS)
    public ResponseEntity<StudentIdentifier> createStudent(@RequestBody Student student) {

        try {
            StudentIdentifier studentIdentifier = studentService.createStudent(student);
            return new ResponseEntity(studentIdentifier, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
