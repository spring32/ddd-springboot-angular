package com.eara.microservices.app.student.domain.vo;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serializable;
import java.util.function.Function;

//@Embeddable
@EqualsAndHashCode
@ToString
public class StudentIdentifier implements Serializable {

    private static final long serialVersionUID = 7535529425843736964L;

//    @Column(name = "id")
    private Long value;

    protected StudentIdentifier() {

    }

    public StudentIdentifier(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    public String getAsString() {
        Function<Long, String> convertFn = anId -> Long.toString(anId);
        return convertFn.apply(this.value);
    }
}
