package com.eara.microservices.app.student.domain.vo;

import com.eara.microservices.app.student.domain.StudentDomainException;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.validator.routines.EmailValidator;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@EqualsAndHashCode
@ToString
public class EmailAddress implements Serializable, ValueObject, Comparable<EmailAddress> {

    private static final EmailValidator validator = EmailValidator.getInstance();

    private static final long serialVersionUID = -6905596049997566470L;

    @Column(name = "email")
    private final String value;

    private EmailAddress() {
        this.value = null;
    }

    public EmailAddress(String value) throws StudentDomainException {
        if (!validator.isValid(value)) {
            throw new StudentDomainException("Invalid email");
        }

        this.value = value;
    }

    public EmailAddress change(String value) throws StudentDomainException {
        return new EmailAddress(value);
    }

    public String emailAddressAsString() {
        return this.value;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || getClass() != o.getClass()) return false;
        EmailAddress that = (EmailAddress) o;

        return Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public int compareTo(EmailAddress other) {
        return 0;
    }
}
