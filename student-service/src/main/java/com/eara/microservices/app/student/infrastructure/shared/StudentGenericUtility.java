package com.eara.microservices.app.student.infrastructure.shared;

import com.eara.microservices.app.student.domain.vo.FullName;

import java.util.function.Function;

public class StudentGenericUtility {
    public static <T extends FullName, R> R getFullNameReversed(T fullName, Function<T, R> fn) {
        return fn.apply(fullName);
    }
}
