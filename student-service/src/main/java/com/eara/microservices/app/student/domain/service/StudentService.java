package com.eara.microservices.app.student.domain.service;

import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    StudentIdentifier createStudent(Student student);
    List<Student> getStudents();
    Optional<Student> getStudentById(StudentIdentifier studentId);
}
