package com.eara.microservices.app.student.infrastructure.repository;

import com.eara.microservices.app.student.infrastructure.entity.StudentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentJpaRepository extends CrudRepository<StudentEntity, Long> {

}
