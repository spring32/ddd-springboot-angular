package com.eara.microservices.app.student.application.rest;

import org.springframework.stereotype.Component;

@Component
public class StudentLinks {

    public static final String LIST_STUDENTS = "/students/all";
    public static final String GET_STUDENT = "/students/{studentId}";
    public static final String ADD_STUDENTS = "/students/student";
}
