package com.eara.microservices.app.student.domain;

import com.eara.microservices.app.student.domain.vo.EmailAddress;
import com.eara.microservices.app.student.domain.vo.FullName;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class Student {
    private StudentIdentifier id;
    private FullName fullName;
    private EmailAddress email;

    private Student () {}

    public Student(final StudentIdentifier id, final FullName fullName, final EmailAddress email) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
    }

    public void changeFullName(final FullName fullName) {

    }

    public void changeLastName(String lastName) {

    }

    public void changeFirstName(String firstName) {

    }

    public void changeEmailAddress(String email) {

    }

    public String getFullNameReversed() {
        return this.fullName.reversedFullName();
    }

    public String getStandardFullName() {
        return this.fullName.standardFullName();
    }
}
