package com.eara.microservices.app.student.infrastructure.entity;

import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.vo.EmailAddress;
import com.eara.microservices.app.student.domain.vo.FullName;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "students", schema = "uo_student_service")
@Access(AccessType.FIELD)
@Data()
@NoArgsConstructor
public class StudentEntity {

    @EmbeddedId
    private StudentIdentifier studentId;

    @Embedded
    private FullName fullName;

    @Embedded
    private EmailAddress email;

    @Column(name = "created_at")
    private Date createdAt;

    public StudentEntity(Student student) {
        this.studentId = student.getId();
        this.fullName = student.getFullName();
        this.email = student.getEmail();
    }

    public StudentIdentifier getId() {
        return studentId;
    }

    @PrePersist
    public void prePersist() {
        this.createdAt = new Date();
    }

    public Student toStudent() {
        Student student = new Student(studentId, fullName, email);
        return student;
    }
}
