package com.eara.microservices.app.student.domain.vo;

import com.eara.microservices.app.student.infrastructure.shared.StudentGenericUtility;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.function.Function;

@Embeddable
@EqualsAndHashCode
@ToString
public class FullName implements Serializable, ValueObject, Comparable<FullName> {

    private static final long serialVersionUID = -981919129615409788L;

    @Column(name = "first_name")
    private final String firstName;

    @Column(name = "last_name")
    private final String lastName;

    private FullName() {
        this.firstName = null;
        this.lastName = null;
    }

    public FullName(String firstName, String lastName) {
        Assert.notNull(firstName, "Firstname should not be null");
        Assert.notNull(lastName, "Lastname should not be null");

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String standardFullName() {
        Function<FullName, String> standardNameFn = (fullName) -> new StringBuilder()
                .append(fullName.firstName.toUpperCase())
                .append(" ")
                .append(fullName.lastName.toUpperCase())
                .toString();

        return  StudentGenericUtility.getFullNameReversed(this, standardNameFn);
    }

    public String reversedFullName() {
        Function<FullName, String> reversedFn = (fullName) -> new StringBuilder()
                .append(fullName.lastName.toUpperCase())
                .append(" ")
                .append(fullName.firstName.toUpperCase())
                .toString();

        return  StudentGenericUtility.getFullNameReversed(this, reversedFn);
    }

    @Override
    public int compareTo(FullName other) {
        return 0;
    }
}
