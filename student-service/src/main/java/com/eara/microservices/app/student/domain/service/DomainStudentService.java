package com.eara.microservices.app.student.domain.service;

import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.StudentDomainException;
import com.eara.microservices.app.student.domain.repository.StudentRepository;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class DomainStudentService implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public StudentIdentifier createStudent(Student student) throws StudentDomainException {

       return studentRepository.save(student).getId();
    }

    @Override
    public List<Student> getStudents() {
        return studentRepository.getAllStudents();
    }

    @Override
    public Optional<Student> getStudentById(StudentIdentifier studentId) {
        return studentRepository.getStudentById(studentId);
    }
}
