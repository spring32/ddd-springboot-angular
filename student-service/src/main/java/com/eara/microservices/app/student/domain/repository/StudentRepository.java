package com.eara.microservices.app.student.domain.repository;

import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;

import java.util.List;
import java.util.Optional;

/**
 * Ports (for Driven Adapters) are interfaces that define contracts
 * which must be implemented by infrastructure adapters in order to
 * execute some action more related to technology detaiils rather
 * the business logic.
 * Ports act like abstractions for technology details that business
 * logic does not care about.
 *
 * - Port are basically just interfaces that define what has to be
 *   done and donàt care about how it is done.
 * - Ports should be created to fit the Domain needs, not simply
 *   mimic the tools APIs.
 * - Mock implementations can be passed to ports while testing. Mocking
 *   makes the tests faster and independent from the environment.
 */
public interface StudentRepository {
    Optional<Student> getStudentById(StudentIdentifier studentId);
    List<Student> getAllStudents();
    Student save(Student student);
    Student update(StudentIdentifier studentId, Student student);
    boolean delete(StudentIdentifier id);
}
