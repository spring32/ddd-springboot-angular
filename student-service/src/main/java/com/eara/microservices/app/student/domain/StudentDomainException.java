package com.eara.microservices.app.student.domain;

public class StudentDomainException extends RuntimeException {
    public StudentDomainException(final String message) {
        super(message);
    }
}
