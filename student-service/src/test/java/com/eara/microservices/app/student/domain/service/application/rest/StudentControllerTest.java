package com.eara.microservices.app.student.domain.service.application.rest;

import com.eara.microservices.app.student.application.rest.StudentController;
import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.StudentDomainException;
import com.eara.microservices.app.student.domain.service.StudentService;
import com.eara.microservices.app.student.domain.vo.EmailAddress;
import com.eara.microservices.app.student.domain.vo.FullName;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @MockBean is used to mock away the business logic, to avoid
 * test integration between controller and business logic, but between
 * controller and the HTTP layer.
 *
 * @ObjectMapper (provided by Spring) is used to map to and from JSON,
 *
 * @MockMvc instance is used to simulate HTTP requests.
 *
 * @author Ernesto A. Rodriguez Acosta
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = StudentController.class)
public class StudentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StudentService studentService;

    @DisplayName("Test Mock studentController.getAllStudents Http 200")
    @Test
    public void whenGetStudents_thenReturnsHttpStatusOk() throws Exception {
        Student student = new Student(
                new StudentIdentifier(UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE),
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        List<Student> mockStudentsList = Arrays.asList(student);

        when(studentService.getStudents()).thenReturn(mockStudentsList);

        this.mockMvc.perform(get("/api/students/all"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].fullNameReversed", is(student.getFullName().reversedFullName())));
    }

    @DisplayName("Test Mock studentController.getStudentById Http 200")
    @Test
    public void whenGetStudentById_thenReturnsHttpStatusOk() throws Exception {
        Long id = UUID.randomUUID()
                .getMostSignificantBits() & Long.MAX_VALUE;

        StudentIdentifier studentId = new StudentIdentifier(id);

        Student student = new Student(
                studentId,
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        when(studentService.getStudentById(studentId)).thenReturn(Optional.of(student));

        this.mockMvc.perform(get("/api/students/{studentId}", id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id.value").value(id));
    }

    @DisplayName("Test Mock studentController.getStudentById Http 404")
    @Test
    public void whenGetStudentByUnexistentId_thenReturnsHttpStatusFailed() throws Exception {
        Long id = UUID.randomUUID()
                .getMostSignificantBits() & Long.MAX_VALUE;

        StudentIdentifier studentId = new StudentIdentifier(id);

        Student student = new Student(
                studentId,
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        when(studentService.getStudentById(studentId)).thenReturn(Optional.of(student));

        this.mockMvc.perform(get("/api/students/{studentId}", 2L))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @DisplayName("Test Mock studentController.createStudent Http 201")
    @Test
    public void whenSaveStudent_thenReturnsHttpStatusOk() throws Exception {

        Long id = UUID.randomUUID()
                .getMostSignificantBits() & Long.MAX_VALUE;

        StudentIdentifier studentId = new StudentIdentifier(id);

        Student student = new Student(
                studentId,
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        when(studentService.createStudent(any(Student.class))).thenReturn(studentId);

        this.mockMvc.perform(
                post("/api/students/student")
                .content(asJsonString(student))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.value").exists());
    }

    @DisplayName("Test Mock studentController.createStudent Http 201")
    @Test
    public void whenSaveInvalidStudent_thenReturnsHttpStatus500() throws Exception {

        Long id = UUID.randomUUID()
                .getMostSignificantBits() & Long.MAX_VALUE;

        StudentIdentifier studentId = new StudentIdentifier(id);

        Student student = new Student(
                studentId,
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        when(studentService.createStudent(any(Student.class))).thenReturn(studentId);
//        when(studentService.createStudent(any(Student.class))).thenThrow(StudentDomainException.class);

        this.mockMvc.perform(
                post("/api/students/student")
                        .content(asJsonString(null))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.value").doesNotExist());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}