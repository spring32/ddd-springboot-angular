package com.eara.microservices.app.student.domain.service;

import com.eara.microservices.app.student.domain.Student;
import com.eara.microservices.app.student.domain.StudentDomainException;
import com.eara.microservices.app.student.domain.repository.StudentRepository;
import com.eara.microservices.app.student.domain.vo.EmailAddress;
import com.eara.microservices.app.student.domain.vo.FullName;
import com.eara.microservices.app.student.domain.vo.StudentIdentifier;
import com.eara.microservices.app.student.infrastructure.entity.StudentEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import static org.mockito.Mockito.mock;

public class StudentDomainServiceUnitTest {

    private StudentRepository studentRepository;
    private DomainStudentService tested;

    @BeforeEach
    void setMockOutput() {
        studentRepository = mock(StudentRepository.class);
        tested = new DomainStudentService(studentRepository);
    }

    @DisplayName("Test Mock studentService.getStudentById")
    @Test
    void shouldFindStudent_thenReturnAnOptional() throws StudentDomainException {
        final StudentIdentifier id = new StudentIdentifier(getLongValue().get());

        final Student student = new Student(
                id,
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        when(tested.getStudentById(id)).thenReturn(Optional.of(student));

        Optional<Student> result = tested.getStudentById(id);
        assertEquals(student, result.get());
    }

    @DisplayName("Test Mock studentService.getStudentByIdAndReturnEmptyOptional")
    @Test
    void shouldNotFindStudent_thenReturnAnEmptyOptional() throws StudentDomainException {

        final StudentIdentifier id = new StudentIdentifier(2L);

        when(tested.getStudentById(id)).thenReturn(null);

        Optional<Student> result = tested.getStudentById(id);

        assertEquals(Optional.empty(), result.get());

    }

    @DisplayName("Test Mock studentService.getStudentByIdAndReturnEmptyOptional")
    @Test
    void shouldFindAllStudents_thenReturnAnStudentList() throws StudentDomainException {

    }

    @DisplayName("Test Mock studentService.createStudent")
    @Test
    void shouldSaveStudent_thenReturnStudentId() throws StudentDomainException {
        final StudentIdentifier id = new StudentIdentifier(getLongValue().get());

        final Student student = new Student(
                id,
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        when(studentRepository.save(any(Student.class))).thenReturn(student);

        StudentIdentifier result = tested.createStudent(student);

        assertEquals(id, result);
    }

    @DisplayName("Test Mock studentService.createStudent")
    @Test()
    void shouldTryToSaveStudent_thenReturnsError() throws StudentDomainException {
        final StudentIdentifier id = new StudentIdentifier(getLongValue().get());

        final Student student = new Student(
                id,
                new FullName("ernesto", "acosta"),
                new EmailAddress("coco@gmail.com"));

        final Student student2 = new Student(null, null, null);

        when(studentRepository.save(any(Student.class))).thenThrow(StudentDomainException.class);

        Assertions.assertThrows(Exception.class, () -> tested.createStudent(student2));
    }

    private Supplier<Long> getLongValue() {
        return () -> UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
    }
}
